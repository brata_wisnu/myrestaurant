package com.aplikasi.myrestaurant;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class RiwayatActivity extends AppCompatActivity {

    Activity activity;
    TextView empty;
    ListView listView;
    ImageButton btnBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_riwayat_pesanan);
        activity = this;
        btnBack = findViewById(R.id.btnBack);
        empty = findViewById(R.id.empty);
        listView = findViewById(R.id.listview);

        listView.setAdapter(null);
        listView.setVisibility(View.VISIBLE);

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(activity, MainActivity.class));
            }
        });

        getListData();

    }

    public void getListData() {
        SQLiteDatabase database = activity.openOrCreateDatabase("DBRestaurant", Context.MODE_PRIVATE, null);
        ArrayList<HashMap<String, String>> list = new ArrayList<HashMap<String, String>>();

        // TODO: LIST MAKANAN
        database.execSQL("CREATE TABLE IF NOT EXISTS TB_DAFTAR_PESANAN(number INTEGER PRIMARY KEY AUTOINCREMENT, ID_PESANAN TEXT, NAMA_PELANGGAN TEXT, NO_MEJA TEXT, WAKTU_PESAN TEXT, PESANAN TEXT);");
        Cursor cursorMakanan = database.rawQuery("select * from TB_DAFTAR_PESANAN", null);
        if (cursorMakanan.moveToFirst()) {
            do {
                HashMap<String, String> pesanan = new HashMap<>();
                String strID = cursorMakanan.getString(cursorMakanan.getColumnIndex("ID_PESANAN"));
                String strUsername = cursorMakanan.getString(cursorMakanan.getColumnIndex("NAMA_PELANGGAN"));
                String strMeja = cursorMakanan.getString(cursorMakanan.getColumnIndex("NO_MEJA"));
                String strWaktu = cursorMakanan.getString(cursorMakanan.getColumnIndex("WAKTU_PESAN"));
                String strPesanan = cursorMakanan.getString(cursorMakanan.getColumnIndex("PESANAN"));

                pesanan.put("id_", strID);
                pesanan.put("username", strUsername);
                pesanan.put("meja", strMeja);
                pesanan.put("waktu", strWaktu);
                pesanan.put("pesanan", strPesanan);
                list.add(pesanan);

            } while (cursorMakanan.moveToNext());
            empty.setVisibility(View.GONE);
            listView.setVisibility(View.VISIBLE);
        } else {
            empty.setVisibility(View.VISIBLE);
            listView.setVisibility(View.GONE);
        }

        ListAdapter adapter = new ListAdapter(activity, list,
                R.layout.item_riwayat, new String[]{"id_", "username", "meja", "waktu", "pesanan"},
                new int[]{R.id.linID, R.id.tvUsername, R.id.tvMeja, R.id.tvWaktu});
        listView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
        ((BaseAdapter) listView.getAdapter()).notifyDataSetChanged();
    }


    // TODO: List Adapter Barcode Scanning
    public class ListAdapter extends SimpleAdapter {
        private Context mContext;
        public LayoutInflater inflater = null;
        private int Nomor = 0;

        public ListAdapter(Context context, List<? extends Map<String, ?>> data, int resource, String[] from, int[] to) {
            super(context, data, resource, from, to);
            mContext = context;
            inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View vi = convertView;
            if (convertView == null)
                vi = inflater.inflate(R.layout.item_riwayat, null);

            HashMap<String, Object> data = (HashMap<String, Object>) getItem(position);

            final LinearLayout _id = vi.findViewById(R.id.linID);
            final TextView _meja = vi.findViewById(R.id.tvMeja);
            final TextView _username = vi.findViewById(R.id.tvUsername);
            final TextView _waktu = vi.findViewById(R.id.tvWaktu);

            final String strID = (String) data.get("id_");
            final String strMeja = (String) data.get("meja");
            final String strUsername = (String) data.get("username");
            final String strWaktu = (String) data.get("waktu");
            final String strPesanan = (String) data.get("pesanan");

            _id.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(activity, DetailRiwayatActivity.class);
                    Utilities.putPref("strID", strID, activity);
                    Utilities.putPref("username", strUsername, activity);
                    Utilities.putPref("meja", strMeja, activity);
                    startActivity(intent);

                }
            });

            _id.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                    builder.setMessage("Anda yakin ingin menghapus?")
                            .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int id) {

                                    SQLiteDatabase database = mContext.openOrCreateDatabase("DBRestaurant", Context.MODE_PRIVATE, null);
                                    // TODO: DELETE PESANAN
                                    database.execSQL("CREATE TABLE IF NOT EXISTS TB_DAFTAR_PESANAN(number INTEGER PRIMARY KEY AUTOINCREMENT, ID_PESANAN TEXT, NAMA_PELANGGAN TEXT, NO_MEJA TEXT, WAKTU_PESAN TEXT, PESANAN TEXT);");
                                    database.execSQL("DELETE FROM TB_DAFTAR_PESANAN where ID_PESANAN = '" + strID + "';");

//                                    // TODO: DELETE MINUMAN
//                                    database.execSQL("CREATE TABLE IF NOT EXISTS TB_MINUMAN(number INTEGER PRIMARY KEY AUTOINCREMENT, NAMA_MINUMAN TEXT, ID_PESANAN TEXT,DETAIL TEXT, HARGA TEXT, JUMLAH TEXT);");
//                                    database.execSQL("DELETE FROM TB_MINUMAN where ID_PESANAN = '" + strID + "';");
//                                    // TODO: DELETE MAKANAN
//                                    database.execSQL("CREATE TABLE IF NOT EXISTS TB_MAKANAN(number INTEGER PRIMARY KEY AUTOINCREMENT, NAMA_MAKANAN TEXT,ID_PESANAN TEXT, DETAIL TEXT, HARGA TEXT, JUMLAH TEXT);");
//                                    database.execSQL("DELETE FROM TB_MAKANAN where ID_PESANAN'" + strID + "';");

                                    getListData();
                                    dialog.dismiss();
                                }
                            })
                            .setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            })
                            .show();
                    return true;
                }
            });
            _meja.setText(strMeja);
            _username.setText(strUsername);
            _waktu.setText(Utilities.ConvertDateFormat(strWaktu));


            return vi;
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(activity, MainActivity.class));
    }
}
