package com.aplikasi.myrestaurant;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    Activity activity;
    Spinner spName;
    EditText etUsername;
    Button btnTentang, btnRiwayat, btnMenu;
    boolean doubleBackToExitPressedOnce = false;
    String[] noMeja = {"Meja 1", "Meja 2", "Meja 3", "Meja 4", "Meja 5", "Meja 6", "Meja 7", "Meja 8", "Meja 9", "Meja 10"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        activity = this;
        etUsername  = (EditText) findViewById(R.id.etUsername);
        spName      = (Spinner) findViewById(R.id.sp_name);
        btnRiwayat  = (Button) findViewById(R.id.btnRiwayat);
        btnTentang  = (Button) findViewById(R.id.btnTentang);
        btnMenu     = (Button) findViewById(R.id.btnMenu);

        btnTentang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(activity,TentangActivity.class));
            }
        });

        btnRiwayat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(activity,RiwayatActivity.class));
            }
        });

        btnMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String strUsername = etUsername.getText().toString().trim();
                String strMeja = spName.getSelectedItem().toString();

                if (strUsername.equals("")){
                    etUsername.setError("tidak boleh kosong");
                    return;
                }

                Utilities.putPref("username",strUsername,activity);
                Utilities.putPref("meja",strMeja,activity);
                Intent intent = new Intent(activity, MenuRestaurant.class);
                Utilities.putPref("activity","order",activity);
                startActivity(intent);

                Utilities.putPref("strID",Utilities.currentDateFormat(),activity);
            }
        });

        final List<String> listMeja = new ArrayList<>(Arrays.asList(noMeja));
        // Initializing an ArrayAdapter
        final ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(
                this,R.layout.spinner_item,listMeja);

        spinnerArrayAdapter.setDropDownViewResource(R.layout.spinner_item);
        spName.setAdapter(spinnerArrayAdapter);


        // mengeset listener untuk mengetahui saat item dipilih
        spName.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                // memunculkan toast + value Spinner yang dipilih (diambil dari adapter)
                //Toast.makeText(activity, spinnerArrayAdapter.getItem(i), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                Utilities.ShowLog("exit", "finishAffinity");
                ActivityCompat.finishAffinity(activity);
            } else {
                Utilities.ShowLog("exit", "finish");
                activity.finish();
            }
            return;
        }
        this.doubleBackToExitPressedOnce = true;
        Utilities.ShowToast(this, getResources().getString(R.string.double_click_exit));
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }
}
