package com.aplikasi.myrestaurant;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class DetailPesananActivity extends AppCompatActivity {

    Activity activity;
    TextView empty, tvHargaTotal, tvMeja, tvUsername;
    ListView listView;
    Button btnSubmit;
    ImageButton btnBack;
    JSONArray arrayPesanan;
    int TotalPesanan = 0;
    String strID,strUsername, strMeja;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_pesanan);
        activity = this;
        btnBack = findViewById(R.id.btnBack);
        btnSubmit = findViewById(R.id.btnSubmit);
        tvHargaTotal = findViewById(R.id.tvTotal);
        tvUsername = findViewById(R.id.tvUsername);
        tvMeja = findViewById(R.id.tvMeja);
        empty = findViewById(R.id.empty);
        listView = findViewById(R.id.listview);

        strID = Utilities.getPref("strID", activity) != null ? Utilities.getPref("strID", activity) : "";
        strUsername = Utilities.getPref("username", activity) != null ? Utilities.getPref("username", activity) : "";
        strMeja = Utilities.getPref("meja", activity) != null ? Utilities.getPref("meja", activity) : "";

        tvUsername.setText("Pemesan: " + strUsername.toUpperCase() + " (" + strMeja + ")");
        tvMeja.setText("Nomor: " + strMeja);
        tvMeja.setVisibility(View.GONE);

        listView.setAdapter(null);
        listView.setVisibility(View.VISIBLE);

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(), "Pesanan diproses..", Toast.LENGTH_SHORT).show();
                Toast.makeText(getApplicationContext(), "Pesanan berhasil", Toast.LENGTH_SHORT).show();
                if (arrayPesanan != null) {
                    SQLiteDatabase database = openOrCreateDatabase("DBRestaurant", Context.MODE_PRIVATE, null);
                    String waktu_pesan = Utilities.currentDateFormat();

                    // TODO: INSERT TO TABLE DAFTAR PESANAN
                    database.execSQL("CREATE TABLE IF NOT EXISTS TB_DAFTAR_PESANAN(number INTEGER PRIMARY KEY AUTOINCREMENT, ID_PESANAN TEXT, NAMA_PELANGGAN TEXT, NO_MEJA TEXT, WAKTU_PESAN TEXT, PESANAN TEXT);");
                    database.execSQL("DELETE FROM TB_DAFTAR_PESANAN where ID_PESANAN = '" + strID + "';");
                    database.execSQL("INSERT OR REPLACE INTO TB_DAFTAR_PESANAN (ID_PESANAN,NAMA_PELANGGAN, NO_MEJA ,WAKTU_PESAN, PESANAN) VALUES('"+strID+"','" + strUsername + "','" + strMeja + "','" + waktu_pesan + "','" + arrayPesanan.toString() + "');");
                    startActivity(new Intent(activity, MainActivity.class));
                }
            }
        });

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(activity, KonfirmasiPesananActivity.class));
            }
        });

        getListData();

    }

    public void getListData() {
        SQLiteDatabase database = activity.openOrCreateDatabase("DBRestaurant", Context.MODE_PRIVATE, null);
        ArrayList<HashMap<String, String>> list = new ArrayList<HashMap<String, String>>();
        arrayPesanan = new JSONArray();

        // TODO: LIST MAKANAN
        database.execSQL("CREATE TABLE IF NOT EXISTS TB_MAKANAN(number INTEGER PRIMARY KEY AUTOINCREMENT, NAMA_MAKANAN TEXT,ID_PESANAN TEXT,DETAIL TEXT, HARGA TEXT, JUMLAH TEXT);");
        int TotalMakanan = 0;
        Cursor cursorMakanan = database.rawQuery("select * from TB_MAKANAN where ID_PESANAN = '"+strID+"'", null);
        if (cursorMakanan.moveToFirst()) {

            do {

                HashMap<String, String> pesananMakanan = new HashMap<>();
                String strID = cursorMakanan.getString(cursorMakanan.getColumnIndex("number"));
                String strMakanan = cursorMakanan.getString(cursorMakanan.getColumnIndex("NAMA_MAKANAN"));
                String strDetail = cursorMakanan.getString(cursorMakanan.getColumnIndex("DETAIL"));
                String strHarga = cursorMakanan.getString(cursorMakanan.getColumnIndex("HARGA"));
                String strJumlah = cursorMakanan.getString(cursorMakanan.getColumnIndex("JUMLAH"));
                Utilities.ShowLog("MINUMAN", strMakanan + " jml: " + strJumlah);

                int subTotalMakanan = Integer.parseInt(strJumlah.replace(".", "")) * (Integer.parseInt(strHarga.replace(".", "")));
                pesananMakanan.put("id_", strID);
                pesananMakanan.put("itemName", strMakanan);
                pesananMakanan.put("detail", strDetail);
                pesananMakanan.put("harga", strHarga);
                pesananMakanan.put("jumlah", strJumlah);
                pesananMakanan.put("sub_total", "" + subTotalMakanan);
                pesananMakanan.put("type", "makanan");
                list.add(pesananMakanan);      try {
                    arrayPesanan.put(new JSONObject().put("id_",strID)
                            .put("itemName",strMakanan)
                            .put("detail",strDetail)
                            .put("harga",strHarga)
                            .put("jumlah",strJumlah)
                            .put("",subTotalMakanan));
                } catch (JSONException e) {
                    e.printStackTrace();
                }


                Utilities.ShowLog("list", pesananMakanan.toString());
                TotalMakanan = TotalMakanan + subTotalMakanan;
            } while (cursorMakanan.moveToNext());
        }

        // TODO: LIST MINUMAN
        database.execSQL("CREATE TABLE IF NOT EXISTS TB_MINUMAN(number INTEGER PRIMARY KEY AUTOINCREMENT, NAMA_MINUMAN TEXT,ID_PESANAN TEXT,DETAIL TEXT, HARGA TEXT, JUMLAH TEXT);");

        Cursor cursorMinuman = database.rawQuery("select * from TB_MINUMAN where ID_PESANAN = '"+strID+"'", null);
        int TotalMinuman = 0;
        if (cursorMinuman.moveToFirst()) {
            //mengecek data sqlite tabel minuman
            do {
                HashMap<String, String> pesananMinuman = new HashMap<>();
                String strID = cursorMinuman.getString(cursorMinuman.getColumnIndex("number"));
                String strMinuman = cursorMinuman.getString(cursorMinuman.getColumnIndex("NAMA_MINUMAN"));
                String strDetail = cursorMinuman.getString(cursorMinuman.getColumnIndex("DETAIL"));
                String strHarga = cursorMinuman.getString(cursorMinuman.getColumnIndex("HARGA"));
                String strJumlah = cursorMinuman.getString(cursorMinuman.getColumnIndex("JUMLAH"));
                Utilities.ShowLog("MINUMAN", strMinuman + " jml: " + strJumlah);


                int subTotalMinuman = Integer.parseInt(strJumlah.replace(".", "")) * (Integer.parseInt(strHarga.replace(".", "")));
                pesananMinuman.put("id_", strID);
                pesananMinuman.put("itemName", strMinuman);
                pesananMinuman.put("detail", strDetail);
                pesananMinuman.put("harga", strHarga);
                pesananMinuman.put("jumlah", strJumlah);
                pesananMinuman.put("sub_total", "" + subTotalMinuman);
                pesananMinuman.put("type", "minuman");
                list.add(pesananMinuman);
                try {
                    arrayPesanan.put(new JSONObject().put("id_",strID)
                            .put("itemName",strMinuman)
                            .put("detail",strDetail)
                            .put("harga",strHarga)
                            .put("jumlah",strJumlah)
                            .put("",subTotalMinuman));
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                TotalMinuman = TotalMinuman + subTotalMinuman;

            } while (cursorMinuman.moveToNext());
        }

        TotalPesanan = TotalMakanan + TotalMinuman;
        Utilities.ShowLog("totalpesaan", "" + TotalPesanan);

        //Format Rupiah
        Locale localeID = new Locale("in", "ID");
        NumberFormat formatRupiah = NumberFormat.getCurrencyInstance(localeID);
        tvHargaTotal.setText(formatRupiah.format((double) TotalPesanan));

        ListAdapter adapter = new ListAdapter(activity, list,
                R.layout.item_menu, new String[]{"id_", "itemName", "detail", "harga", "jumlah", "sub_total", "type"},
                new int[]{R.id.linID, R.id.tvItem, R.id.tvHarga, R.id.tvDetail});
        listView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
        ((BaseAdapter) listView.getAdapter()).notifyDataSetChanged();
        
    }
    

    // TODO: List Adapter Barcode Scanning
    public class ListAdapter extends SimpleAdapter {
        private Context mContext;
        public LayoutInflater inflater = null;
        private int Nomor = 0;

        public ListAdapter(Context context, List<? extends Map<String, ?>> data, int resource, String[] from, int[] to) {
            super(context, data, resource, from, to);
            mContext = context;
            inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View vi = convertView;
            if (convertView == null)
                vi = inflater.inflate(R.layout.item_detail, null);

            HashMap<String, Object> data = (HashMap<String, Object>) getItem(position);

            final LinearLayout _id = vi.findViewById(R.id.linID);
            final TextView _nama_item = vi.findViewById(R.id.tvItem);
            final TextView _harga = vi.findViewById(R.id.tvHarga);
            final TextView _detail_item = vi.findViewById(R.id.tvDetail);
            final TextView _sub_harga = vi.findViewById(R.id.tvSubHarga);
            final ImageButton _btnHapus = vi.findViewById(R.id.btnDelete);

            final String strID = (String) data.get("id_");
            final String strNamaItem = (String) data.get("itemName");
            final String strDetailItem = (String) data.get("detail");
            final String strHarga = ((String) data.get("harga")).replace(".", "");
            final String strJumlah = (String) data.get("jumlah");
            final String strSubTotal = ((String) data.get("sub_total")).replace(".", "");
            final String strType = (String) data.get("type");

            _btnHapus.setVisibility(View.GONE);

            _btnHapus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                    builder.setMessage("Anda yakin ingin menghapus?")
                            .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int id) {
                                    SQLiteDatabase database = activity.openOrCreateDatabase("DBRestaurant", Context.MODE_PRIVATE, null);
                                    if (strType.equals("minuman")) {
                                        // TODO: DELETE MINUMAN
                                        database.execSQL("CREATE TABLE IF NOT EXISTS TB_MINUMAN(number INTEGER PRIMARY KEY AUTOINCREMENT, NAMA_MINUMAN TEXT,ID_PESANAN TEXT,DETAIL TEXT, HARGA TEXT, JUMLAH TEXT);");
                                        database.execSQL("DELETE FROM TB_MINUMAN where ID_PESANAN = '"+strID+"' AND NAMA_MINUMAN ='" + strNamaItem + "';");
                                    } else {
                                        // TODO: DELETE MAKANAN
                                        database.execSQL("CREATE TABLE IF NOT EXISTS TB_MAKANAN(number INTEGER PRIMARY KEY AUTOINCREMENT, NAMA_MAKANAN TEXT,ID_PESANAN TEXT,DETAIL TEXT, HARGA TEXT, JUMLAH TEXT);");
                                        database.execSQL("DELETE FROM TB_MAKANAN where ID_PESANAN = '"+strID+"' AND NAMA_MAKANAN ='" + strNamaItem + "';");
                                    }
                                    getListData();
                                    dialog.dismiss();
                                }
                            })
                            .setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            })
                            .show();
                }
            });

            _nama_item.setText(strNamaItem);

            //Format Rupiah
            Locale localeID = new Locale("in", "ID");
            NumberFormat formatRupiah = NumberFormat.getCurrencyInstance(localeID);

            _harga.setText(strJumlah + " x @" + formatRupiah.format((double) Integer.parseInt(strHarga)));
            _detail_item.setText("(" + strDetailItem + ")");
            _sub_harga.setText(formatRupiah.format((double) Integer.parseInt(strSubTotal)));
            if (strDetailItem.equals("")) {
                _detail_item.setVisibility(View.GONE);
            } else _detail_item.setVisibility(View.VISIBLE);

            return vi;
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(activity, KonfirmasiPesananActivity.class));
    }
}
