package com.aplikasi.myrestaurant;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SuppressLint("ValidFragment")
public class TabMakanan extends Fragment {

    TextView empty;
    ListView listView;
    Button btnPesanan;
    String strID, intentActivity;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.layout_makanan, container, false);

        intentActivity = Utilities.getPref("activity", getContext()) != null ? Utilities.getPref("activity", getContext()) : "";
        strID = Utilities.getPref("strID", getContext()) != null ? Utilities.getPref("strID", getContext()) : "";
        btnPesanan = v.findViewById(R.id.btnPesanan);
        empty = v.findViewById(R.id.empty);
        listView = v.findViewById(R.id.listview);

        listView.setAdapter(null);
        listView.setVisibility(View.VISIBLE);

        ArrayList<HashMap<String, String>> list = new ArrayList<HashMap<String, String>>();
        HashMap<String, String> data1 = new HashMap<>();
        //ayam bakar
        data1.put("id_", "1");
        data1.put("nama_makanan", "Ayam Bakar Taliwang");
        data1.put("harga", "32.000");
        data1.put("detail", "ayam bakar sambal ijo");
        list.add(data1);

        //ayam goreng
        HashMap<String, String> data2 = new HashMap<>();
        data2.put("id_", "2");
        data2.put("nama_makanan", "Ayam goreng Taliwang");
        data2.put("harga", "32.000");
        data2.put("detail", "ayam goreng sambal ijo");
        list.add(data2);

        //gurame
        HashMap<String, String> data3 = new HashMap<>();
        data3.put("id_", "3");
        data3.put("nama_makanan", "Gurami Bakar Taliwang");
        data3.put("harga", "40.000");
        data3.put("detail", "gurami bakar sambal ijo");
        list.add(data3);

        //plecing kangkung
        HashMap<String, String> data4 = new HashMap<>();
        data4.put("id_", "4");
        data4.put("nama_makanan", "Plecing kangkung");
        data4.put("harga", "10.000");
        data4.put("detail", "");
        list.add(data4);

        //lalapan
        HashMap<String, String> data5 = new HashMap<>();
        data5.put("id_", "5");
        data5.put("nama_makanan", "Lalapan");
        data5.put("harga", "10.000");
        data5.put("detail", "");
        list.add(data5);

        //Nasi
        HashMap<String, String> data6 = new HashMap<>();
        data6.put("id_", "6");
        data6.put("nama_makanan", "Nasi Putih");
        data6.put("harga", "5.000");
        data6.put("detail", "");
        list.add(data6);

        ListAdapter adapter = new ListAdapter(getContext(), list,
                R.layout.item_menu, new String[]{"id_", "nama_makanan", "harga", "detail"},
                new int[]{R.id.linID, R.id.tvItem, R.id.tvHarga, R.id.tvDetail});
        listView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
        ((BaseAdapter) listView.getAdapter()).notifyDataSetChanged();

        btnPesanan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (JumlahItemPesanan() > 0) {
                    if (intentActivity.equals("order")) {
                        startActivity(new Intent(getContext(), KonfirmasiPesananActivity.class));
                    } else {
                        startActivity(new Intent(getContext(), DetailRiwayatActivity.class));
                    }
                } else {
                    Toast.makeText(getContext(), "Pilih pesanan terlebih dulu", Toast.LENGTH_SHORT).show();

                }
            }
        });


        btnPesanan.setText("Pesanan (" + JumlahItemPesanan() + " item)");
        return v;
    }


    // TODO: List Adapter Barcode Scanning
    public class ListAdapter extends SimpleAdapter {
        private Context mContext;
        public LayoutInflater inflater = null;

        public ListAdapter(Context context, List<? extends Map<String, ?>> data, int resource, String[] from, int[] to) {
            super(context, data, resource, from, to);
            mContext = context;
            inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View vi = convertView;
            if (convertView == null)
                vi = inflater.inflate(R.layout.item_menu, null);

            HashMap<String, Object> data = (HashMap<String, Object>) getItem(position);

            final LinearLayout _id = vi.findViewById(R.id.linID);
            final TextView _nama_makanan = vi.findViewById(R.id.tvItem);
            final TextView _harga = vi.findViewById(R.id.tvHarga);
            final TextView _detail_item = vi.findViewById(R.id.tvDetail);

            final String strID = (String) data.get("id_");
            final String strMakanan = (String) data.get("nama_makanan");
            final String strHarga = (String) data.get("harga");
            final String strDetailItem = (String) data.get("detail");

            _id.setOnLongClickListener(new View.OnLongClickListener() {

                @Override
                public boolean onLongClick(View v) {
                    // TODO Auto-generated method stub
                    InputDialogPopup(getContext(), strMakanan, strHarga, strDetailItem);
                    return true;
                }
            });

            _id.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    InputDialogPopup(getContext(), strMakanan, strHarga, strDetailItem);
                }
            });

            _nama_makanan.setText(strID + ". " + strMakanan);
            _harga.setText("Rp. " + strHarga);
            _detail_item.setText("(" + strDetailItem + ")");
            if (strDetailItem.equals("")) {
                _detail_item.setVisibility(View.GONE);
            } else _detail_item.setVisibility(View.VISIBLE);

            return vi;
        }
    }


    // TODO: POPUP INPUT
    public void InputDialogPopup(Context context, String nama_makanan, final String harga, final String detail) {

        //Popup
        final Dialog _dialog;
        Context _context;
        final EditText _etJumlah;
        final TextView _tvNama, _tvHarga, _tvDetail;
        Button _btnOk, _btnCancel;

        _context = context;
        _dialog = new Dialog(_context);
        _dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        _dialog.setContentView(R.layout.popup_input);
        _tvNama = _dialog.findViewById(R.id.tvItem);
        _tvHarga = _dialog.findViewById(R.id.tvHarga);
        _tvDetail = _dialog.findViewById(R.id.tvDetail);
        _etJumlah = _dialog.findViewById(R.id.etJumlah);
        _btnOk = _dialog.findViewById(R.id.btnOK);
        _btnCancel = _dialog.findViewById(R.id.btnCancel);

        _tvNama.setText(nama_makanan);
        _tvHarga.setText("Rp. " + harga);
        _tvDetail.setText("(" + detail + ")");

        if (detail.equals("")) {
            _tvDetail.setVisibility(View.GONE);
        } else _tvDetail.setVisibility(View.VISIBLE);

        _btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String strJumlah = _etJumlah.getText().toString().trim().equals("") ? "0" : _etJumlah.getText().toString().trim();
                String strNama = _tvNama.getText().toString().trim();
                _dialog.dismiss();

                SQLiteDatabase database = getActivity().openOrCreateDatabase("DBRestaurant", Context.MODE_PRIVATE, null);
                if (!strJumlah.equals("0")) {
                    // TODO: INSERT TO TABLE MAKANAN
                    database.execSQL("CREATE TABLE IF NOT EXISTS TB_MAKANAN(number INTEGER PRIMARY KEY AUTOINCREMENT, NAMA_MAKANAN TEXT, ID_PESANAN TEXT, DETAIL TEXT, HARGA TEXT, JUMLAH TEXT);");
                    database.execSQL("DELETE FROM TB_MAKANAN where ID_PESANAN = '" + strID + "' AND NAMA_MAKANAN ='" + strNama + "';");
                    database.execSQL("INSERT OR REPLACE INTO TB_MAKANAN (NAMA_MAKANAN, ID_PESANAN, DETAIL, HARGA, JUMLAH) VALUES('" + strNama + "','" + strID + "','" + detail + "','" + harga + "','" + strJumlah + "');");

                } else {
                    // TODO: DELETE MAKANAN
                    database.execSQL("CREATE TABLE IF NOT EXISTS TB_MAKANAN(number INTEGER PRIMARY KEY AUTOINCREMENT, NAMA_MAKANAN TEXT, ID_PESANAN TEXT,DETAIL TEXT, HARGA TEXT, JUMLAH TEXT);");
                    database.execSQL("DELETE FROM TB_MAKANAN where ID_PESANAN = '" + strID + "' AND NAMA_MAKANAN ='" + strNama + "';");
                }

                btnPesanan.setText("Pesanan (" + JumlahItemPesanan() + " item)");
            }
        });

        _btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _dialog.cancel();
            }
        });

        _dialog.show();

    }


    public int JumlahItemPesanan() {
        SQLiteDatabase database = getActivity().openOrCreateDatabase("DBRestaurant", Context.MODE_PRIVATE, null);
        database.execSQL("CREATE TABLE IF NOT EXISTS TB_MAKANAN(number INTEGER PRIMARY KEY AUTOINCREMENT, NAMA_MAKANAN TEXT,ID_PESANAN TEXT, DETAIL TEXT, HARGA TEXT, JUMLAH TEXT);");

        int jumlahMakanan = 0;
        Cursor cursorMakanan = database.rawQuery("select * from TB_MAKANAN where ID_PESANAN = '" + strID + "'", null);

        if (cursorMakanan.moveToFirst()) {
            jumlahMakanan = cursorMakanan.getCount();

            //mengecek data sqlite tabel makanan
            do {
                String strMakanan = cursorMakanan.getString(cursorMakanan.getColumnIndex("NAMA_MAKANAN"));
                String strJumlah = cursorMakanan.getString(cursorMakanan.getColumnIndex("JUMLAH"));
                Utilities.ShowLog("Makanan", strMakanan + " jml: " + strJumlah);

            } while (cursorMakanan.moveToNext());
        }

        database.execSQL("CREATE TABLE IF NOT EXISTS TB_MINUMAN(number INTEGER PRIMARY KEY AUTOINCREMENT, NAMA_MINUMAN TEXT,ID_PESANAN TEXT, DETAIL TEXT, HARGA TEXT, JUMLAH TEXT);");

        int jumlahMinuman = 0;
        Cursor cursorMinuman = database.rawQuery("select * from TB_MINUMAN where ID_PESANAN = '" + strID + "'", null);

        if (cursorMinuman.moveToFirst()) {
            jumlahMinuman = cursorMinuman.getCount();

            //mengecek data sqlite tabel minuman
            do {
                String strMinum = cursorMinuman.getString(cursorMinuman.getColumnIndex("NAMA_MINUMAN"));
                String strJumlah = cursorMinuman.getString(cursorMinuman.getColumnIndex("JUMLAH"));
                Utilities.ShowLog("MINUMAN", strMinum + " jml: " + strJumlah);

            } while (cursorMinuman.moveToNext());
        }

        return (jumlahMakanan + jumlahMinuman);
    }
}
