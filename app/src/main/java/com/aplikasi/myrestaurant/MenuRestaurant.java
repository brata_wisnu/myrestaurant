package com.aplikasi.myrestaurant;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTabHost;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

public class MenuRestaurant extends FragmentActivity {
    Activity activity;
    Button btnPesanan;
    ImageButton btnKembali;
    private FragmentTabHost tabHost;
    String strID;
    String intentActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_restaurant);
        activity = this;

        btnPesanan = findViewById(R.id.btnPesanan);
        btnKembali = findViewById(R.id.btnBack);
        tabHost = findViewById(android.R.id.tabhost); // initiate TabHost
        intentActivity =  Utilities.getPref("activity", activity) != null ? Utilities.getPref("activity", activity) : "";

        tabHost.setup(activity, getSupportFragmentManager(), android.R.id.tabcontent);

        tabHost.addTab(
                tabHost.newTabSpec("Makanan").setIndicator("Makanan", null),
                TabMakanan.class, null);
        tabHost.addTab(
                tabHost.newTabSpec("Minuman").setIndicator("Minuman", null),
                TabMinuman.class, null);

        btnKembali.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clearData();
                if (intentActivity.equals("order")) {
                    startActivity(new Intent(activity, MainActivity.class));
                }else {
                    startActivity(new Intent(activity, DetailRiwayatActivity.class));
                }
            }
        });

        btnPesanan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (intentActivity.equals("order")) {
                    startActivity(new Intent(activity, KonfirmasiPesananActivity.class));
                }else {
                    startActivity(new Intent(activity, DetailRiwayatActivity.class));
                }
            }
        });

    }

    public void clearData() {
        SQLiteDatabase database = openOrCreateDatabase("DBRestaurant", Context.MODE_PRIVATE, null);
        // TODO: DELETE MAKANAN
        database.execSQL("CREATE TABLE IF NOT EXISTS TB_MAKANAN(number INTEGER PRIMARY KEY AUTOINCREMENT, NAMA_MAKANAN TEXT, ID_PESANAN TEXT,DETAIL TEXT, HARGA TEXT, JUMLAH TEXT);");
        database.execSQL("DELETE FROM TB_MAKANAN where ID_PESANAN ='" + strID + "';");
        // TODO: DELETE MAKANAN
        database.execSQL("CREATE TABLE IF NOT EXISTS TB_MINUMAN(number INTEGER PRIMARY KEY AUTOINCREMENT, NAMA_MINUMAN TEXT, ID_PESANAN TEXT,DETAIL TEXT, HARGA TEXT, JUMLAH TEXT);");
        database.execSQL("DELETE FROM TB_MINUMAN where ID_PESANAN ='" + strID + "';");
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        clearData();

        if (intentActivity.equals("order")) {
            startActivity(new Intent(activity, MainActivity.class));
        } else {
            startActivity(new Intent(activity, DetailRiwayatActivity.class));
        }
    }

}
